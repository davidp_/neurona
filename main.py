from PIL import Image
from os import listdir
from random import random
import numpy as np
import matplotlib.pyplot as plt

def ls(ruta):
    return listdir(ruta)

class generateGrid:
    def __init__(self, path):
        lista = []
        banco = ls(path)
        n = 0
        print("banco"+str(banco))
        for x in banco:
            print("x"+str(x))
            imagen = Image.open(path+x)
            #ancho  = int(imagen.size[0]/5)
            ancho = 30
            #alto = int(imagen.size[1]/5)
            alto = 30
            for si in range (5):
                for gh in range (8):
                    caja = (gh*ancho, si*alto, (gh*ancho) + ancho, (si*alto) + alto)
                    print (caja)
                    print ('tamaño: ' + str(imagen.size))
                    region = imagen.crop(caja)
                    lista.append(region)
                    name = 'cuadrado'+str(n+1)+'.png'
                    print (name)
                    region.save("cachos/"+name)
                    n = n + 1

class generarHistograma:
    def __init__(self, path):
        cachos = ls(path)
        #for i in cachos:
        print (str(cachos[0]))
        img = Image.open(path+cachos[0])
        #img.show()
        self.histogramaR(img)
    
    def getRed(self, redVal):
        return '#%02x%02x%02x' % (redVal, 0, 0)

    def getGreen(self, greenVal):
        return '#%02x%02x%02x' % (0, greenVal, 0)

    def getBlue(self, blueVal):
        return '#%02x%02x%02x' % (0, 0, blueVal)

    def histogramaR(self, region): 
        mos = [int(random()*100) for _ in range(3000)]
        #registros = list(region.getdata())
        histogram = region.histogram() 
        l1 = histogram[0:256]
        l2 = histogram[256:512]
        l3 = histogram[512:768]
        fig, ax = plt.subplots()
        ax.hist(l1, 256, ec='red', fc='none', lw=1.5, histtype='step', label='Dist A')
        #plt.figure(0)
        # R histogram
        #for i in range(0, 256):
        #    plt.bar(i, l2[i], edgecolor=self.getGreen(i),alpha=0.3)            

        # G histogram
        plt.figure(1)
        for i in range(0, 256):
            plt.bar(i, l2[i], edgecolor=self.getGreen(i),alpha=0.3)

        # B histogram
        plt.figure(2)
        for i in range(0, 256):
            plt.bar(i, l3[i], edgecolor=self.getBlue(i),alpha=0.3)

        plt.show()
        #print(type(registros))
        #analizar pixeles de la region
        #plt.title('Histograma')
        #plt.hist(mos, bins=60, alpha=1, edgecolor = 'red',  linewidth=1)
        #plt.grid(True)
        #plt.show()
        #plt.clf()
    
    def histogramaG(self, region): 
        mos = [int(random()*100) for _ in range(3000)]
        plt.title('Histograma')
        plt.hist(mos, bins=60, alpha=1, edgecolor = 'black',  linewidth=1)
        plt.grid(True)
        plt.show()
        plt.clf()
    
    def histogramaB(self, region): 
        mos = [int(random()*100) for _ in range(3000)]
        plt.title('Histograma')
        plt.hist(mos, bins=60, alpha=1, edgecolor = 'black',  linewidth=1)
        plt.grid(True)
        plt.show()
        plt.clf()



#obj = generateGrid("banco/")
obj1 = generarHistograma("cachos/")